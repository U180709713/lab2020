import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };

		int moveCount = 0;
		int currentPlayer = 0;
		char symbol;
		while (moveCount < 9){

			int row,col;
			printBoard(board);
			do {

				System.out.print("Player "+(currentPlayer+1)+" enter row number:");
				row = reader.nextInt();
				System.out.print("Player "+(currentPlayer+1)+" enter column number:");
				col = reader.nextInt();

			}while(!(row>0 && row<4 && col>0 && col<4 && board[row-1][col-1] == ' ' ));

			board[row-1][col-1] = (currentPlayer == 0 ? 'X' : 'O');

			currentPlayer = (currentPlayer+1) %2;
			symbol = board[row-1][col-1];

			if(checkHorizantal(board,row,col)){
				System.out.println("-------------------------");
				System.out.println(symbol+" won that Game :)");
				System.out.println("-------------------------");
				break;
			}

			if(checkVertical(board,row,col)){
				System.out.println("-------------------------");
				System.out.println(symbol+" won that Game :)");
				System.out.println("-------------------------");
				break;
			}

			if(row-1 == 0 && col-1 ==0 || row-1 == 0 && col -1 == 2 || row-1 == 2 && col-1 == 0 || row-1 == 2 && col-1 == 2 || row-1 == 1 && col-1 == 1 )
				if(checkDiagonal(board,row,col)){
					System.out.println("-------------------------");
					System.out.println(symbol+" won that Game :)");
					System.out.println("-------------------------");
					break;
			}
		}
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");
		}

	}

	public static boolean checkHorizantal(char[][] board,int rowLast, int colLast) {

		char symbol = board[rowLast - 1][colLast - 1];
		boolean win = true;
		for (int col = 0; col < 3; col++) {

			if (board[rowLast - 1][col] != symbol) {
				win = false;
				break;
			}

		}

		if(win)
			return true;
		return false;
	}

	public static boolean checkVertical(char[][] board,int rowLast, int colLast){
		char symbol = board[rowLast - 1][colLast - 1];
		boolean win = true;
		for (int row = 0; row < 3; row++) {
				if(board[row][colLast-1] != symbol){
				win = false;
				break;
			}

		}

		if(win)
			return true;
		return false;
	}

	public static boolean checkDiagonal(char[][] board, int rowLast, int colLast){

		char symbol = board[rowLast - 1][colLast - 1];
		boolean win = true;
		if (rowLast-1 == 0 && colLast-1 ==0)
			for (int col = 1; col < 3; col++) {
				if (board[col][col] != symbol)
					win = false;
				break;
			}
		else if (rowLast-1 == 0 && colLast -1 == 2)
			for (int i = 1; i < 3; i++){
				if (board[rowLast-1  + i ][colLast-1 - i] != symbol) {
					win = false;
					break;
				}
			}
		else if(rowLast-1 == 2 && colLast-1 == 0)
			for(int i = 1; i < 3; i++){
				if (board[(rowLast-1) - i ][(colLast-1) + i] != symbol){
					win = false;
					break;
				}
			}
		else if(rowLast-1 == 2 && colLast-1 == 2)
			for (int col = 2; col > -1; col--){
				if (board[col][col] != symbol) {
					win = false;
					break;
				}
			}
		else if(rowLast-1 == 1 && colLast-1 == 1) {
			for (int i = 0; i < 3; i += 2) {
				if (board[i][i] != symbol) {
					win = false;
					break;
				}
			}

			for (int row = 0, col = 2 ; row< 3; row += 2, col -=2){
				if (board[row][col] != symbol) {
					win = false;
					break;
				}
			}
		}

		if(win)
			return true;
		return false;
	}
}

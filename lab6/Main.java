public class Main{
    public static void main(String[] args) {

        Point p = new Point(4,6);
        Rectangle r = new Rectangle(p,5,9);
       
        System.out.println("############################################");
        System.out.println("Rectangle Area: "+r.area());
        System.out.println("Rectangle Perimeter: "+r.perimeter());
        System.out.println("############################################");

        Point[] points = r.corners();
        for (int i=0; i<points.length; i++){
            System.out.println("Point "+(i+1)+": "+points[i].xCoord + "," + points[i].yCoord);

        }

        Circle c = new Circle(p,10);
        
        
        System.out.println("");
        System.out.println("############################################");
        System.out.println("Circle Perimeter: "+c.perimeter());        
        System.out.println("Circle Area :: "+c.area());
        System.out.println("############################################");

        Point p2 = new Point(4,20);
        Circle c2 = new Circle(p2,5);
        System.out.println("");
        System.out.println("############################################");
        System.out.println("Intersection of 2 Circle (c2): "+c.intersect(c2));
        System.out.println("############################################");




    }
}

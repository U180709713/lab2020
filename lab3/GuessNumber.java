import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		
		
		System.out.println("Hi! I'm thinking of a number between 0 and 99.");
		System.out.print("Can you guess it: ");

		
		
		int count = 0;
		int guess;
		do {
			System.out.print("Type -1 to quit or give me a guess: ");
			guess = reader.nextInt();
			count++;

			if (guess == -1)
				System.out.println("Sorry, the number was " + number);
			
			else if (guess == number)
				System.out.println("Congratulations! You won after "+ count +" attempts!" );

			else if (guess < number)
				System.out.println("Mine ise greater than your guess.");

			else if (guess > number)
				System.out.println("Mine ise less than your guess.");
			
			else
				System.out.println("Sorry");
			
			
			

		}while(number != guess && guess != -1);
		
		
		
		
		reader.close(); //Close the resource before exiting
	}
	
	
}

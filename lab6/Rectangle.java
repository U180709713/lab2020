public class Rectangle{

    Point topLeft;
    int sideA, sideB;

    //sideA is longer side

    public Rectangle(Point p, int a, int b){
        this.sideA = a;
        this.sideB = b;
        this.topLeft = p;

    }

    public int area(){
        //Calculates Area of Rectangle
        return this.sideA * this.sideB;
    }

    public int perimeter(){
        //Calculates perimeter of Rectangle
        return 2 * (this.sideA + this.sideB);
    }

    public Point[] corners(){
        //Returns 4 corner of rectangle
        Point topRight = new Point(this.topLeft.xCoord + this.sideA, this.topLeft.yCoord);
        Point botRight = new Point(topRight.xCoord, topRight.yCoord - this.sideB);
        Point botLeft = new Point(botRight.xCoord - this.sideA, botRight.yCoord);

        Point[] pointArray = new Point[4];
        pointArray[0] = this.topLeft;
        pointArray[1] = topRight;
        pointArray[2] = botLeft;
        pointArray[3] = botRight;

        return pointArray;







    }

}